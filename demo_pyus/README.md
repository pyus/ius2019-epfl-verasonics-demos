[ius2019]: https://attend.ieee.org/ius-2019/
[python]: https://www.python.org
[matlab]: https://www.mathworks.com/products/matlab.html
[verasonics]: https://verasonics.com/
[vantage]: https://verasonics.com/vantage-systems/
[pyus]: https://pyus_link
[epfl]: https://www.epfl.ch/
[lts5]: https://lts5www.epfl.ch
[cuda]: https://developer.nvidia.com/cuda-toolkit
[cuda10]: https://developer.nvidia.com/cuda-10.0-download-archive
[openmp]: https://openmp.com
[fftw3]: http://fftw.org
[pycuda]: https://documen.tician.de/pycuda/

# Using [PyUS], a GPU-accelerated [Python] package for ultrasound image reconstruction, on a [Vantage] system

[PyUS] is an open source GPU-accelerated Python package for ultrasfast ultrasound
image reconstruction developed in the [Signal Processing Laboratory (LTS5)][lts5] at the
[École polytechnique fédérale de Lausanne (EPFL)][epfl]. 

## Installation
PyUS depends on the [NVIDIA CUDA Toolkit 10.0][cuda10] and the [FFTW3] library so you will
have to install them first.
Note that you don't _require_ to have a GPU in your system but for now you 
still need to have [CUDA 10.0][cuda10] installed for [PyUS] to work.
To do so, download you prefered installer from the [NVIDIA website][cuda10]
and follow the instructions [here](https://docs.nvidia.com/cuda/archive/10.0/)
to install it. You will also need to install [FFTW3] which you can do by 
downloading the archive from the [FFTW website](http://www.fftw.org/download.html)
and install it as described [here](http://www.fftw.org/fftw3_doc/Installation-on-Unix.html#Installation-on-Unix).
For [PyUS] to work, you need to configure [FFTW3] as follows:
```
./configure --enable-float --enable-shared --enable-openmp
```
Next you can install [PyUS] in your environment:
``` bash
conda create --name demo-pyus python=3.7
conda activate demo-pyus
pip install pyus==0.2.1
```

Note that PyUS is still in developement and may be subject to API changes in the
future, so for the sake of reproducibility it is best to specifically install the
0.2.1 version (which was used during the demonstrations at the [2019 IEEE IUS][ius2019]). 
Now add the provided scipts to your [MATLAB] path and you are all set!

## Calling PyUS from MATLAB
To get started with calling Python functions from MATLAB, check out our tutorial
[here](../demo_python/README.md).

### Initialization
In the `SetUpGE9LD_demoPyUS.m` we set up the acquisition as usual, except that
we won't use the default image reconstruction because we will use [PyUS] to 
do so, so we don't need to define the `PData`, `Recon`, 
`ReconInfo` and `Resource.DisplayWindow` structures. We only need a `Process`
structure which will call our [`recon_demoPyUS`](recon_demoPyUS.m) function.

Since [MATLAB] cannot store [Python] objects into `.mat` files, we cannot do the
required initialization and setup for [PyUS] in the usual `SetUpGE9LD_demoPyUS.m`
file. We will have to move them in the `VSX.m` file, which performs all kinds
of initialization and setup steps for the hardware before launching the sequence.
We can then do our needed initializations just before the `%% Run sequence.` line.
There are several things done there:
- Import the different [Python] packages needed
- Initalize [PyCUDA] which is used in [PyUS] to handle data transfer between 
[Python] and the GPU memory (please visit the [PyCUDA website][pycuda] for
more information)
- Set up the different variables and objects needed for image reconstruction.
[PyUS] uses object oriented programming hence we need to create a beamformer object
as well as delays objects (one per steered plane wave), an interpolation and 
an apodization objects that will be called during the reconstruction. This 
offers us a modular reconstruction pipeline that we can easily change without 
stopping and restarting the sequence. 
- Pre-allocate some GPU arrays for performance concerns
- Create the figures so we just have to update their content once the sequence
is launched, increasing a bit the display performances.

### Reconstruction
The image reconstruction is handled in the [`recon_demoPyUS`](recon_demoPyUS.m)
function. Note that we heavily use the `evalin` function which allows us to get
variables that were defined in the base workspace (the ones defined in VSX during
the initialization). This way, we can use all the objects we initialized there
and just call their methods.

The different steps involved in the reconstruction are:
- Transfer the reordered acquired data to [Python] then to the GPU
- Choose a reconstruction method based on GUI input
- Call [PyUS] to perform the chosen reconstruction
- Convert the reconstructed RF-image to B-mode
- Transfer the B-mode image back to [MATLAB]
- Display

For this demonstration, we used a custom display window and GUI defined
respectively in the `VSX_demoPyUS.m` and `vsx_gui_epfl.m` files that you can
find on the Script Repository of the Verasonics Community.  