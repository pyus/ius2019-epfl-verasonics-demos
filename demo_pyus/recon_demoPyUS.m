function recon_demoPyUS(RData)

% Get python packages from base workspace
np = evalin('base', 'np');
pyus_signal_utils = evalin('base', 'pyus_signal_utils');

% Get pyus beamformer, its associated objects and useful arrays and variables
pyus_das = evalin('base', 'pyus_beamformer');
interpolator_left = evalin('base', 'interpolator_left');
interpolator_right = evalin('base', 'interpolator_right');
delays = evalin('base', 'delays');
apodization_left = evalin('base', 'apodization_left');
apodization_right = evalin('base', 'apodization_right');
Trans = evalin('base', 'Trans');
slice_display = evalin('base', 'slice_display');
nb_angles = evalin('base', 'nb_angles');

%% Prepare data
Receive = evalin('base', 'Receive');

% Number of samples per acquisition is computed as:
% acqLen = 2 * (Receive(1).endDepth - Receive(1).startDepth) * Receive(1).samplesPerWave;
acqLen = Receive(1).endSample;
frameLen = Receive(nb_angles).endSample;
data_2d = RData(1:frameLen, Trans.Connector); % copy here since we discard the trailing zeros

% Flatten data
data_1D = reshape(data_2d, 1, []); % normally no copy here because same array

% Convert to numpy array (only 1D row vector can be passed from matlab to python)
np_data_1D = np.array(data_1D);

% Reshape data with numpy
new_shape = py.tuple([int32(1), int32(Trans.numelements), int32(nb_angles), int32(acqLen)]);
transpose_axes = {int32(0), int32(2), int32(1), int32(3)};
np_data = np.ascontiguousarray(np_data_1D.reshape(new_shape).transpose(transpose_axes), np.float32);

% Copy data to gpu (reuse same array, no allocation)
data_gpu = evalin('base', 'data_gpu');
data_gpu.set(np_data);

%% Reconstruction
% 1 = None
% 2 = 1 PW
% 3 = CPWC
% Triger CPWC reconstruction depending on the GUI input
nb_angles_recon = int32(evalin('base', 'nb_angles_recon'));
idx_angle0 = idivide(int32(nb_angles), int32(2)); % 0 based
idx_start_angle = idx_angle0 - idivide(nb_angles_recon, int32(2)); % 0 based
LeftRecon = evalin('base', 'LeftRecon');
RightRecon = evalin('base', 'RightRecon');
bmode_none = evalin('base', 'bmode_none');
scale_fact_1pw = 1000;
scale_fact_cpwc = nb_angles_recon * scale_fact_1pw;

%% Reconstruct left B-mode image
rf_image_gpu = evalin('base', 'rf_image_gpu'); % get preallocated gpu array

if (LeftRecon == 1)
    % No reconstruction
    bmode_left_disp = bmode_none;
else
    if (LeftRecon == 2)
        wks = py.dict(pyargs('bmf', pyus_das, ... 
                             'data', data_gpu, ...
                             'np', np, ...
                             'idx', idx_angle0, ...
                             'interp', interpolator_left, ...
                             'delays', delays, ...
                             'apod', apodization_left, ...
                             'image', rf_image_gpu));

        % Single plane wave
        py.eval('bmf.reconstruct(data[:,np.newaxis,idx,:,:], interp, [delays[idx]], apod, image)', wks);
        image_rf_left = rf_image_gpu;
        scale_factor = scale_fact_1pw;

    elseif (LeftRecon == 3)
        wks = py.dict(pyargs('bmf', pyus_das, ...
                             'data', data_gpu, ...
                             'np', np, ...
                             'idx', idx_start_angle, ...
                             'nb_angles', nb_angles_recon, ...
                             'interp', interpolator_left, ...
                             'delays', delays, ...
                             'apod', apodization_left, ...
                             'image', rf_image_gpu));
        
        % Coherent plane wave compounding
        py.eval('bmf.reconstruct(data[:,idx:idx + nb_angles,:,:], interp, delays[idx:idx + nb_angles], apod, image)', wks);
        image_rf_left = rf_image_gpu;
        scale_factor = scale_fact_cpwc;

    end

    % Convert to B-mode
    bmode_gpu = evalin('base', 'bmode_gpu'); % get preallocated gpu array
    kwargs = pyargs('rf_data', image_rf_left, ...
                    'norm', 'factor', ...
                    'norm_factor', scale_factor, ...
                    'bmode', bmode_gpu);
    pyus_signal_utils.rf2bmode(kwargs);

    bmode_left = bmode_gpu.get();
    % Convert numpy to python array and transfer to Matlab
    bmode_left_disp = single(py.array.array('f', bmode_left.tobytes()));
    bmode_left_disp = reshape(bmode_left_disp, bmode_left.shape{4}, bmode_left.shape{2});
    bmode_left_disp = bmode_left_disp(1:slice_display:end, :);
end

%% Reconstruct right B-mode image
if (RightRecon == 1)
    % No reconstruction
    bmode_right_disp = bmode_none;
else
    if (RightRecon == 2)
        wks = py.dict(pyargs('bmf', pyus_das, ...
                             'data', data_gpu, ...
                             'np', np, ...
                             'idx', idx_angle0, ...
                             'interp', interpolator_right, ...
                             'delays', delays, ...
                             'apod', apodization_right, ...
                             'image', rf_image_gpu));

        % Single plane wave
        py.eval('bmf.reconstruct(data[:,np.newaxis,idx,:,:], interp, [delays[idx]], apod, image)', wks);
        image_rf_right = rf_image_gpu;
        scale_factor = scale_fact_1pw;

    elseif (RightRecon == 3)
        wks = py.dict(pyargs('bmf', pyus_das, ...
                             'data', data_gpu, ...
                             'np', np, ...
                             'idx', idx_start_angle, ...
                             'nb_angles', nb_angles_recon, ...
                             'interp', interpolator_right, ...
                             'delays', delays, ...
                             'apod', apodization_right, ...
                             'image', rf_image_gpu));
        
        % Coherent plane wave compounding
        py.eval('bmf.reconstruct(data[:,idx:idx + nb_angles,:,:], interp, delays[idx:idx + nb_angles], apod, image)', wks);
        image_rf_right = rf_image_gpu;
        scale_factor = scale_fact_cpwc;

    end

    % Convert to B-mode
    bmode_gpu = evalin('base', 'bmode_gpu'); % get preallocated gpu array
    kwargs = pyargs('rf_data', image_rf_right, ...
                    'norm', 'factor', ...
                    'norm_factor', scale_factor, ...
                    'bmode', bmode_gpu);
    pyus_signal_utils.rf2bmode(kwargs);

    bmode_right = bmode_gpu.get();
    % Convert numpy to python array and transfer to Matlab
    bmode_right_disp = single(py.array.array('f', bmode_right.tobytes()));
    bmode_right_disp = reshape(bmode_right_disp, bmode_right.shape{4}, bmode_right.shape{2});
    bmode_right_disp = bmode_right_disp(1:slice_display:end, :);
end

%% Display B-mode images
dbrange = evalin('base', 'vmax - vmin');
title_1pw = sprintf('DAS (1 PW) - %d dB dynamic range\n', dbrange);
title_cpwc = sprintf('DAS (%d PW) - %d dB dynamic range\n', nb_angles_recon, dbrange);
title_none = sprintf('');

switch LeftRecon
    case 1
        title_left = title_none;
    case 2
        title_left = title_1pw;
    case 3
        title_left = title_cpwc;
end

switch RightRecon
    case 1
        title_right = title_none;
    case 2
        title_right = title_1pw;
    case 3
        title_right = title_cpwc;
end

% Check if graphic handle is still valid before plotting, else exit VSX
LaxesHandle = evalin('base', 'LaxesHandle');
if isgraphics(LaxesHandle)
    LaxesHandle.Title.String = title_left;
    LimageHandle = evalin('base', 'LimageHandle');
    LimageHandle.CData = bmode_left_disp;
else
    assignin('base', 'exit', 1);
end

RaxesHandle = evalin('base', 'RaxesHandle');
if isgraphics(RaxesHandle)
    RaxesHandle.Title.String = title_right;
    RimageHandle = evalin('base', 'RimageHandle');
    RimageHandle.CData = bmode_right_disp;
else
    assignin('base', 'exit', 1);
end

%% Update figure
% nocallbacks defers callbacks to the next full drawnow call in VSX, to
% reach a higher frame rate and treat callbacks every 5 frames
% drawnow nocallbacks

% hmm so pause is supposed to be equivalent to a full drawnow, yet it is
% faster than a drawnow and a drawnow nocallbacks, with a cap at ~0.005s
pause(0.001);

return


