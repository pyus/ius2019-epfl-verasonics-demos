[python]: https://www.python.org
[matlab]: https://www.mathworks.com/products/matlab.html
[verasonics]: https://verasonics.com/
[vantage]: https://verasonics.com/vantage-systems/
[numpy]: https://numpy.org/
[scipy]: https://www.scipy.org/

# Calling simple [Python] processes from [MATLAB] on raw data acquired by a [Vantage] system

Here we demonstrate how to call simple [Python] processes from [MATLAB] on data
acquired by a [Verasonics] [Vantage] Research System.
To do so we will base ourselves on the Vantage Sequence Programming Tutorial in
which one can learn step by step how to write a script to acquire and process
data with a [Vantage] system.

## Installation
For this tutorial, you only need a [Python] environment with [NumPy] and [SciPy] installed.
Then add the provided scripts to your [MATLAB] path and you are good to go.
``` bash
conda create --name demo-python python=3.7
conda activate demo-python
pip install numpy scipy
```

## Calling [Python] from [MATLAB]
The best way to get started with [Python] from [MATLAB] is to read the official
[MATLAB documentation](https://www.mathworks.com/help/releases/R2017b/matlab/getting-started-with-python.html).
Note that this tutorial uses MATLAB R2017b so the documentation referred here is 
the R2017b one, some functions may have changed in more recent releases.

### [Python] interpreter
To use a [Python] interpreter from [MATLAB] we first need to specify the interpreter
we want to use. To do so, we call the [`pyversion`](https://www.mathworks.com/help/releases/R2017b/matlab/ref/pyversion.html)
function with the path to our interpreter __before__ calling any [Python] function:
```matlab
pyversion('<path/to/python/interpreter>')
```
Indeed, calling a [Python] function loads the [Python] interpreter in [MATLAB]
and once loaded, it cannot be changed in the current [MATLAB] session, we have to 
restart [MATLAB] and call [`pyversion`](https://www.mathworks.com/help/releases/R2017b/matlab/ref/pyversion.html)
again to change the interpreter.

### Call [Python] function
[Documentation](https://www.mathworks.com/help/releases/R2017b/matlab/calling-python-functions.html)

To call a Python function in MATLAB, we just have to prefix the function call 
with `py.`
```matlab
py.print('Calling Python print function from MATLAB');
```
To use keyword arguments, we need to use the [`pyargs`](https://www.mathworks.com/help/releases/R2017b/matlab/ref/pyargs.html)
function and pass its return value to our [Python] function:
```matlab
kwargs = pyargs('arg1', my_arg1, 'arg2', my_arg2);
py.my_function(kwargs);
```
For more informations on [Python] function arguments, see [here](https://www.mathworks.com/help/releases/R2017b/matlab/matlab_external/python-function-arguments.html).

Some Python [features](https://ch.mathworks.com/help/releases/R2017b/matlab/matlab_external/limitations-to-python-support.html)
and [functions](https://www.mathworks.com/help/releases/R2017b/matlab/matlab_external/how-matlab-represents-python-operators.html)
are not compatible with [MATLAB] and the only way to call them is to use the
[`py.eval()`](https://www.mathworks.com/help/releases/R2017b/matlab/matlab_external/call-python-eval-function.html)
function:
```matlab
wks = py.dict(pyargs('my_arg1', my_arg1, 'my_arg2', my_arg2));
py.eval('my_function(my_arg1, arg2=my_arg2)', wks);
```

### Import a module
To import a [Python] module into [MATLAB], we use the following syntax:
```matlab
module = py.importlib.import_module('my_module');
```
Our [Python] module is now in the [MATLAB] workspace and we can call any function
inside it as we would do in [Python]:
```matlab
module.my_function(arg1, arg2);
```
You may need to add your module to the [Python] path (see [here](https://ch.mathworks.com/help/releases/R2017b/matlab/matlab_external/call-user-defined-custom-module.html)) 
or reload it after modifications (see [here](https://ch.mathworks.com/help/releases/R2017b/matlab/matlab_external/call-modified-python-module.html)).

### Pass data to [Python]
[Documentation](https://ch.mathworks.com/help/releases/R2017b/matlab/python-data-types.html)

The compatible data types and their mappings are listed [here](https://ch.mathworks.com/help/releases/R2017b/matlab/matlab_external/passing-data-to-python.html#buialof-50).
Note that we can only pass 1D (1-by-N) [MATLAB] vectors and only use [Python]
[`array.array`](https://docs.python.org/3.7/library/array.html) to pass data 
between [MATLAB] and [Python] 

A very useful package for scientific computing in [Python] is [NumPy]. To 
transfer data from a [MATLAB] vector to a [NumPy] array, there are several steps:
- Reshape the data to create a 1D vector
  ```matlab
  mat_data_2D = [1 2 3; 4 5 6; 7 8 9];
  mat_data = reshape(mat_data, 1, []);
  ```
- Create a [Python] array from the 1D vector
  ```matlab
  py_data = py.array.array('d', mat_data)
  ```
- Create a [NumPy] array from the [Python] array and reshape it as needed
  ```matlab
  np = py.importlib.import_module('numpy');
  np_data = np.array(py_data);
  np_data_2D = np_data.reshape(int32(3), int32(3));
  ```
  Note that the 2D [MATLAB] vector and [NumPy] array are not ordered in the same
  way, because [MATLAB] uses column major ordering while [Python] uses row major
  ordering, hence to have the exact same matrix between the two languages we
  need to perform a transpose operation. 
  
  > Note: importing [NumPy] may cause a memory allocation error and crash [MATLAB].
  > In this case, we need to use these two lines __before__ importing [NumPy].
  > For more information, see [here](https://stackoverflow.com/questions/56837160/matlab-crashes-on-linux-when-trying-to-import-python-module):
  > ```matlab
  > py.sys.setdlopenflags(int32(10))
  > ssl = py.importlib.import_module('ssl');
  > ```

To get data back from [Python] to [MATLAB], we use the following steps:

- Create a [Python] array from the [NumPy] array. There are several ways to do this
  and in our experience, the fastest way is to use the [`tobytes()`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.tobytes.html)
  method of a [NumPy] array to get the raw data bytes and create a [Python] 
  array from it. We could also use the [`tolist()`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.tolist.html)
  method or use an iterator with [`np.nditer()`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.nditer.html):
  ```matlab
  py_data = py.array.array('d', np_data.tobytes());
  ```
- Create a vector in the [MATLAB] workspace from the [Python] array and reshape
  it as needed:
  ```matlab
  mat_data = double(py_data);
  mat_data = reshape(mat_data, 3, 3);
  ```

## Use Python on data from a [Vantage] system
To demonstrate how to call [Python] functions on data acquired by a 
[Vantage] system, we will start from the set-up script detailed 
in the Vantage Sequence Programming Tutorial on pages 62-63. This script has 
everything needed to launch an asynchronous sequence and run a simple external 
[MATLAB] function on the acquired raw data. We will only change the external
function and use the one provided in this folder to call some Python functions.
To do so, only change the corresponding name in the `Process` structure, here we
use [`pythonExternalFunction`](pythonExternalFunction.m). In this function, we 
use the techniques detailed above to:
- Transfer the reordered acquired data to [Python]
- Compute the envelope by taking the absolute value of the analytical signal 
using [SciPy] and [NumPy]
- Copy the envelopes back to the [MATLAB] workspace and display them

The first part of the function sets up the [Python] modules and the 
figure once. In this tutorial these set-up step is done in the process function
for simplicity, but they can (and should) be done elsewhere, for example in the
`VSX.m` script.

Note that the set-up script provided on Verasonics Community is for the GE 9L-D
transducer and should be adapted to the one you will be  using. 