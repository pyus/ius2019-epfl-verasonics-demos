function pythonExternalFunction(RData)

% Import necessary python packages
persistent np
persistent spsig
if isempty(np)
    % Fix for issue with libcrypto.so (https://stackoverflow.com/questions/56837160/matlab-crashes-on-linux-when-trying-to-import-python-module)
    py.sys.setdlopenflags(int32(10))
    ssl = py.importlib.import_module('ssl');
    % Import modules
    np = py.importlib.import_module('numpy');
    spsig = py.importlib.import_module('scipy.signal');
end

% Evaluate variables in the base workspace
Trans = evalin('base', 'Trans');
Receive = evalin('base', 'Receive');
start_sample = Receive(1).startSample;
end_sample = Receive(1).endSample;
acq_len = end_sample - start_sample + 1;

% Create the figure if it doesn't exist
persistent axesHandle
persistent line_data
persistent line_env
if isempty(axesHandle)||~ishandle(axesHandle)
    figure;
    axesHandle = axes('NextPlot', 'replacechildren');
    axesHandle.XLim = [start_sample, end_sample];
    axesHandle.XLimMode = 'manual';
    axesHandle.YLim = [-1, 1];
    axesHandle.YLimMode = 'manual';
    axesHandle.XLabel.String = 'Samples';
    axesHandle.YLabel.String = 'Normalized amplitude';
%     axesHandle.Title.String = 'Title';
    line_data = plot(axesHandle, ones(acq_len, 1));
    hold(axesHandle, 'on');
    line_env = plot(axesHandle, ones(acq_len, 1));
    hold(axesHandle, 'off');
    drawnow;
end

% Copy and reorder data
data_2D = RData(start_sample:end_sample, Trans.Connector); 

% Normalize for easier display and convert to double
data_2D = double(data_2D) / double(max(max(data_2D)));

% Flatten data (only 1-by-N vectors can be passed to Python)
data_1D = reshape(data_2D, 1, []); % no copy here because same array

% Copy data to numpy array. The data is passed by matlab as a python
% array.array and numpy automatically creates a ndarray from it
% matlab vector -> python array.array -> numpy ndarray
np_data_1D = np.array(data_1D);

% Reshape data with numpy
new_shape = {int32(Trans.numelements), int32(acq_len)}; % this cell array will be converted to a tuple
np_data = np_data_1D.reshape(new_shape);

% Compute envelope in Python
% Use pyargs for keyword arguments
kwargs = pyargs('x', np_data, 'axis', int32(-1));
a_sig = spsig.hilbert(kwargs);
envelope = np.abs(a_sig);

% Get data back from python
% numpy ndarray -> python array.array -> matlab vector
env = double(py.array.array('d', envelope.tobytes())); % could use tolist() or np.nditer()
env = reshape(env, acq_len, Trans.numelements);

% Display (only update the data instead of the whole figure)
channel = 32;
line_data.YData = data_2D(:, channel);
line_env.YData = env(:, channel);
drawnow;

end

