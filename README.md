[ius2019]: https://attend.ieee.org/ius-2019/
[verasonics]: https://verasonics.com
[epfl]: https://www.epfl.ch
[vantage]: https://verasonics.com/vantage-systems/
[lts5]: https://lts5www.epfl.ch
[pyus]: https://gitlab.com/pyus/pyus
[python]: https://www.python.org
[matlab]: https://www.mathworks.com/products/matlab.html
[interp]: https://doi.org/10.1109/42.875199
[slides_pyus]: https://docs.google.com/viewer?url=https://gitlab.com/pyus/ius2019-epfl-verasonics-demos/raw/master/handouts/ius2019_epfl_verasonics_pyus_demo.pdf?inline=false
[slides_dl]: https://docs.google.com/viewer?url=https://gitlab.com/pyus/ius2019-epfl-verasonics-demos/raw/master/handouts/ius2019_epfl_verasonics_dl_demo.pdf?inline=false
[slides_python]: https://docs.google.com/viewer?url=https://gitlab.com/pyus/ius2019-epfl-verasonics-demos/raw/master/handouts/ius2019_epfl_verasonics_python_demo.pdf?inline=false
[flyer]: https://docs.google.com/viewer?url=https://gitlab.com/pyus/ius2019-epfl-verasonics-demos/raw/master/handouts/ius2019_epfl_verasonics_flyer.pdf?inline=false
[miniconda]: https://docs.conda.io/en/latest/miniconda.html

# Short Seminars in Advanced Applications
>Deep learning techniques, GPU-accelerated image reconstruction,
>and Python processes from data acquired on a Vantage system 

## Summary
This repository contains material and codes related to the real-time
demonstrations, implemented on a [Vantage] system, presented by
representatives from [École polytechnique fédérale de Lausanne (EPFL)][epfl],
[Signal Processing Laboratory 5 (LTS5)][lts5], Lausanne, Switzerland during 
the coffee breaks of the [2019 IEEE International Ultrasonics Symposium][ius2019]
in the [Verasonics] booth.

Due to copyright concerns, only files released under the BSD-3 license are
available from here. The [MATLAB] Verasonics related scripts 
(`SetUpGE9LD<...>.m`, `VSX.m` etc.) are available for download on the [Script 
Repository of the Verasonics Community](https://verasonicscommunity.com/portals/?info=86&title=IEEE%20IUS%202019%20EPFL-Verasonics%20demonstrations). 

## Monday, 7 October (afternoon break)
Using [PyUS], a GPU-accelerated [Python] package for ultrasound image
reconstruction, on a [Vantage] system:

- State-of-the-art delay-and-sum beamforming
- Pixel-based parallel on-the-fly computations
- [Generalized B-spline interpolation][interp]

Additional information:
- [Slides][slides_pyus]
- [Code](demo_pyus)

## Tuesday, 8 October (morning break)
Deep-learning-based image reconstruction method —
High-quality ultrasound imaging from a single plane wave:

- Convolutional neural network trained on simulated data
- Robust to experimental data
- Real-time capabilities

Additional information:
- [Slides][slides_dl]

## Wednesday, 9 October (afternoon break)
Calling simple [Python] processes from [MATLAB] on raw data acquired by a
[Vantage] system:

- Importing and using [Python] packages from [MATLAB]
- Data transfer from [Vantage] system to [Python]
- Tutorial on signal processing applications

Additional information:
- [Slides][slides_python]
- [Code](demo_python)

## System requirements
The demonstrations showcased at IUS2019 ran on the following system setup:
- [Vantage] 256 system
- [Ubuntu 16.04](http://releases.ubuntu.com/16.04/)
- Vantage software release 3.4.3
- [MATLAB] R2017b (9.3)
- [Python] 3.7

All of these demonstrations use [Python] so you will need to install it and create a
dedicated environment for each. To do so, we recommend using [Miniconda] as
it provides an easy way to manage your [Python] environments without messing with
your system [Python] installation. Just download the installer and follow the 
installation instructions. Then create an environment, activate it and install 
the dependencies you will need for each demo:
``` bash
conda create -n <env_name> python=3.7
conda activate <env_name>
pip install ...
```

In order to be able to call Python from Matlab, we need to tell Matlab which
Python executable and thus which environment we want to use. To do so, we have to
call __before__ any python command the following [MATLAB] command:
```
pyversion('<path/to/your/python/bin>');
```
in the case of a [Miniconda] installation, we would call
```
pyversion('/home/<username>/miniconda3/envs/<env_name>/bin/python');
```

## Handouts
- [Flyer]

## Acknowledgements
We would like to thank [Verasonics] for giving us the opportunity to demonstrate
our work at their booth during the [2019 IEEE International Ultrasonics 
Symposium][ius2019].